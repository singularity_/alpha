import csv
import numpy as np
import matplotlib.pyplot as plt

font = {
    'family' : 'Arial',
    'size'   : 16
}
plt.rc("font", **font)
plt.rc("axes", titlesize=24, labelsize=20)


X_LABEL = "Timestamp"

time_list = []
ph_list = []
tds_list = []
turb_list = []

csv_file = open("water_data.csv", "r")
reader = csv.reader(csv_file, delimiter=',')
line_count = 0

for record in reader:

    if line_count == 0:
        line_count += 1
    else:
        time_list.append(record[0])
        ph_list.append(float(record[1]))
        tds_list.append(float(record[2]))
        turb_list.append(int(record[3]))
        line_count += 1


################# PH ####################
#########################################

plt.figure(figsize=(12, 8))
plt.plot(time_list, ph_list, 'go-', linewidth=2, markersize=12)
plt.xlabel(X_LABEL, labelpad=20)
plt.ylabel("pH value", labelpad=20)
plt.ylim(ymin=6.5, ymax=8.5)
plt.savefig("graph_ph.pdf")

################# TDS ###################
#########################################

plt.figure(figsize=(12, 8))
plt.plot(time_list, tds_list, 'go-', linewidth=2, markersize=12)
plt.xlabel(X_LABEL, labelpad=20)
plt.ylabel("TDS value (mg/L)", labelpad=20)
plt.ylim(ymin=200, ymax=300)
plt.savefig("graph_tds.pdf")


################ TURB ###################
#########################################

plt.figure(figsize=(12, 8))
plt.plot(time_list, turb_list, 'go-', linewidth=2, markersize=12)
plt.xlabel(X_LABEL, labelpad=20)
plt.ylabel("Turbidity value (NTU)", labelpad=20)
plt.ylim(ymin=-1, ymax=10)
plt.savefig("graph_turb.pdf")
