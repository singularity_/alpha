This is the first message

This is another: Quick actions are text-based shortcuts for common actions that are usually done by selecting buttons or dropdowns in the GitLab user interface. You can enter these commands in the descriptions or comments of issues, epics, merge requests, and commits. Be sure to enter each quick action on a separate line to allow GitLab to properly detect and execute the commands.


